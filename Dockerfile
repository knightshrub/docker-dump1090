FROM archlinux:latest

RUN pacman -Sy
RUN pacman -S --noconfirm \
    git \
    make \
    gcc \
    glibc \
    pkgconf \
    rtl-sdr \
    lighttpd

RUN git clone https://github.com/flightaware/dump1090
WORKDIR dump1090
RUN make BLADERF=no HACKRF=no LIMESDR=no

RUN pacman -Rs --noconfirm \
    git \
    make \
    gcc \
    pkgconf

RUN mkdir -p /srv/www \
    && cp -r public_html_merged/* /srv/www

COPY ./httpd.conf /etc/lighttpd/httpd.conf

WORKDIR /
COPY ./startup.sh .
RUN chmod +x startup.sh
ENTRYPOINT ./startup.sh

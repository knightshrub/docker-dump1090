#!/bin/sh
lsu=$(lsusb -d 0bda:2838)
bus=$(echo $lsu | cut -d' ' -f2)
dev=$(echo $lsu | cut -d' ' -f4 | sed 's/://')
docker build --network host -t dump1090 .
docker run --rm -d --network host \
    --device /dev/bus/usb/$bus/$dev:/dev/bus/usb/$bus/$dev \
    dump1090
